#include <stdio.h>
#include <string.h>
#include <dirent.h>

#include <utils.h>
#include <analyse.h>

static char path[256];

void usage()
{
  unsigned int nb;

  printf("Usage: projectstat [options] directory ...\n");

  printf("Options:\n");

  printf("  -d, --details%n", &nb);
  if(nb > 20) nb = 20;
  printf("%*.s Display more information about given language(s) (comma separated), example: -d c,java,c++\n", 20-nb, " ");

  printf("  -q, --quiet%n", &nb);
  if(nb > 20) nb = 20;
  printf("%*.s Make projectstat quietter\n", 20-nb, " ");

  exit(0);
}

int main(int argc, char **argv)
{
  printf("Project stat\n");
  printf("find statistics about your project.\n\n");

  struct options opt;
  memset(&opt, 0, sizeof(struct options));

#if DEBUG == 0
  if(argc <= 1)
  {
    usage();
  }

  int found_path = 0;

  unsigned int i = 0;
  for(i=1; i<argc; i++)
  {
    if(argv[i][0] == '-')
    {
      if(strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--details") == 0)
      {
        if(i == argc-1) usage();
        opt.details = argv[i+1];
        i++;
      } else if(strcmp(argv[i], "-q") == 0 || strcmp(argv[i], "--quiet") == 0)
      {
        opt.quiet = 1;
      }
    } else
    {
      found_path = 1;
      strncpy(path, argv[i], 255);
      if(i != (argc-1)) usage();
    }
  }

  if(!found_path)
  {
    printf("Please specify a path to analyse.\n");
    usage();
  }
#else
  strcpy(path, ".");
#endif

  if(path[strlen(path)-1] == '/')
  {
    path[strlen(path)-1] = '\0';
  }

  debug_printf("[Options] details: %s | quiet: %d\n", opt.details, opt.quiet);

  analyse_result_t result;
  analyse(path, &result, &opt);
  dump_result(&result, &opt);

  free_result(&result);

  return 0;
}
