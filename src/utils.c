#include <utils.h>

#include <string.h>
#include <stdio.h>

void error(char *reason)
{
  fprintf(stderr, "[ERROR] at %s:%d : %s\n[ERROR] aborting...\n", __FILE__, __LINE__, reason);
  exit(-1);
}

int strendswith(const char *src, const char *end)
{
  if(strlen(src) < strlen(end))
    return 0;
  return strcasecmp(src + (strlen(src) - strlen(end)), end) == 0;
}

size_t filelines(FILE *f)
{
  char c;
  size_t lines = 0;

  while((c = fgetc(f)) != EOF)
  {
    if(c == '\n')
      lines++;
  }

  lines++;
  return lines;
}

int is_binary(const void *data, size_t len)
{
    return memchr(data, '\0', len) != NULL;
}

#define FILEISBIN_LEN 300
int fileisbin(FILE *f)
{
  char *data = malloc(FILEISBIN_LEN + 1);
  size_t sz = fread((void *) data, 1, FILEISBIN_LEN, f);
  rewind(f);
  int res = is_binary((void *) data, sz);
  free(data);
  return res;
}

#ifdef __linux__

#include <sys/stat.h>

size_t filesize(FILE *f)
{
  int fd = fileno(f);
  struct stat st;
  fstat(fd, &st);
  return (size_t) st.st_size;
}

#else

size_t filesize(FILE *f)
{
  fseek(fp, 0L, SEEK_END);
  size_t sz = ftell(fp);
  rewind(f);
  return sz;
}

#endif
