#include <analyse.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#include <utils.h>
#include <common.h>
#include <list.h>

uint8_t lang_compare(void *val, void *lang)
{
  struct lang_info *li = (struct lang_info *) val;

  if(li->lang == *((int *) lang))
  {
    return 1;
  }

  return 0;
}

static void analyse_file(char *project_path, char *folder, char *filename, analyse_result_t *result, struct options *opt)
{
  char path[256];
  strncpy(path, folder, 255);
  strncat(path, "/", 2);
  int remaining = 255 - strlen(path);
  if(remaining < 0 || remaining < strlen(filename))
    error("Path too long");
  strcat(path, filename);

  for(size_t i = 0; i<common_files_len; i++)
  {
    if(strendswith(path, common_files[i].ext))
    {
      if(common_files[i].is_binary)
        return;
      FILE *file = fopen(path, "r");
      if(file == NULL) error("failed to open file");

      size_t lines = filelines(file);

      debug_printf("[INFO] file found : \"%s\" (ext=%s, lines=%lu, lang=%d)\n", path, common_files[i].ext, lines, common_files[i].lang);

      node_t *n = list_find_custom(result->langs, &(common_files[i].lang), lang_compare);
      struct lang_info *li;
      if(n == NULL)
      {
        /* first file for this language */
        li = (struct lang_info *) malloc(sizeof(struct lang_info));
        li->lang = common_files[i].lang;
        li->files = list_create();
        li->files_nb = 0;
        li->lines = 0;
        list_append(result->langs, li);
      } else
      {
        li = (struct lang_info *) n->value;
      }

      li->files_nb++;
      li->lines += lines;

      struct file_info *fi = (struct file_info *) malloc(sizeof(struct file_info));
      fi->type = i;
      fi->lines = lines;
      strncpy(fi->path, (char *) (((uintptr_t) folder) + strlen(project_path)), 255);
      strncat(fi->path, "/", 2);
      remaining = 255 - strlen(fi->path);
      if(remaining < 0 || remaining < strlen(filename))
        error("Path too long");
      strcat(fi->path, filename);

      list_append(li->files, fi);

      result->total_lines += lines;
      result->total_files++;

      fclose(file);
      return;
    }
  }

  /* file is unknown */
  FILE *file = fopen(path, "r");
  if(file == NULL) error("failed to open file");
  int bin = fileisbin(file);

  debug_printf("[INFO] unknown file found \"%s\" (type=%s)\n", path, (bin) ? "binary" : "text");

  if(!bin)
  {
    size_t lines = filelines(file);
    result->unknown_lines += lines;
    result->unknown_files++;
  } else
  {
    result->binary_files++;
  }

  fclose(file);
}

static void analyse_folder(char *project_path, char *path, analyse_result_t *result, struct options *opt)
{
  if(!opt->quiet)
  {
    printf("[INFO] analysing folder \"%s\"\n", path);
  }

  DIR *dir;
  struct dirent *child;

  dir = opendir(path);
  if(dir != NULL)
  {
    while((child = readdir(dir)) != NULL)
    {
      if(child->d_type == DT_DIR)
      {
        if(is_in_blacklist(child->d_name))
        {
          continue;
        }
        char childpath[256];
        strncpy(childpath, path, 255);
        strncat(childpath, "/", 2);
        if((strlen(child->d_name) + strlen(childpath)) > 255)
        {
          error("Path too long");
        }
        strcat(childpath, child->d_name);

        analyse_folder(project_path, childpath, result, opt);
      } else if(child->d_type == DT_REG)
      {
        if(is_in_blacklist(child->d_name))
        {
          continue;
        }
        analyse_file(project_path, path, child->d_name, result, opt);
      }
    }

    closedir(dir);
  } else
  {
    error("failed to open folder");
  }
}

void analyse(char *path, analyse_result_t *result, struct options *opt)
{
  printf("\nAnalysing project \"%s\"...\n", path);

  memset(result, 0, sizeof(analyse_result_t));

  result->project_folder = strdup(path);

  result->langs = list_create();

  result->total_lines = 0;
  result->total_langs = 0;
  result->unknown_lines = 0;
  result->unknown_files = 0;

  analyse_folder(path, path, result, opt);
}

void dump_detail(struct lang_data *lang, size_t langid, analyse_result_t *result)
{
  node_t *n = list_find_custom(result->langs, &langid, lang_compare);
  struct lang_info *li;
  if(n == NULL) return;

  li = (struct lang_info *) n->value;

  /* common infos */
  printf("\nDetails about a specific language : %s\n", lang->name);
  printf("  Total lines : %lu\n", li->lines);
  printf("  Number of files : %lu\n", li->files_nb);
  printf("  Percentage of total project : %.2f%%\n", li->percent);

  /* order file by size */
  if(li->files->length >= 2)
  {
    unsigned int actions = 1;

    node_t *last_node = li->files->head;
    struct file_info *last = (struct file_info *) last_node->value;
    struct file_info *curr;
    while(actions > 0)
    {
      actions = 0;
      last_node = list_start_iterating(li->files);
      last = (struct file_info *) last_node->value;

      while((n = list_iterate(li->files)) != NULL)
      {
        curr = (struct file_info *) n->value;
        if(curr->lines > last->lines)
        {
          list_swap_node(n, last_node);
          actions++;
        }
        last_node = n;
        last = curr;
      }
    }
  }
  /* print files ordered by size (only the last and first FILES_SHOWN) */
  printf("  List of files (ordered by size, relative to project's path) :\n");
  unsigned int i, j, nb;
  unsigned int file_nb_offset = 10;

  if(li->files->length < FILES_SHOWN)
  {
    n = NULL;
    j = 1;
    foreach(li->files, n)
    {
      struct file_info *fi = (struct file_info *) n->value;
      printf("    [%u]%n", j, &nb);
      if(nb > file_nb_offset) nb = file_nb_offset;
      printf("%*.s %s (%lu lines, %.2f%%)\n", file_nb_offset-nb, " ", fi->path, fi->lines, fi->percent);
      j++;
    }
  } else
  {
    unsigned int nb_files = FILES_SHOWN/2;
    /* first FILES_SHOWN/2 */
    n = list_start_iterating(li->files);
    for(i = 0; i<nb_files; i++)
    {
      struct file_info *fi = (struct file_info *) n->value;
      printf("    [%u]%n", i+1, &nb);
      if(nb > file_nb_offset) nb = file_nb_offset;
      printf("%*.s %s (%lu lines, %.2f%%)\n", file_nb_offset-nb, " ", fi->path, fi->lines, fi->percent);
      n = list_iterate(li->files);
    }

    /* skip */
    for(i = 0; i<(li->files->length - FILES_SHOWN); i++)
    {
      n = list_iterate(li->files);
    }
    printf("    ...\n");

    /* last 10 */
    j = li->files->length - (nb_files) + 1;
    for(i = 0; i<nb_files; i++)
    {
      struct file_info *fi = (struct file_info *) n->value;
      printf("    [%u]%n", j, &nb);
      if(nb > file_nb_offset) nb = file_nb_offset;
      printf("%*.s %s (%lu lines, %.2f%%)\n", file_nb_offset-nb, " ", fi->path, fi->lines, fi->percent);
      n = list_iterate(li->files);
      j++;
    }
  }
}

void free_result(analyse_result_t *result)
{
  /* free list content */
  foreach(result->langs, n)
  {
    list_destroy(((struct lang_info *) n->value)->files);
    free(n->value);
  }

  list_free(result->langs);
  free((void *) result->project_folder);
}

int is_in_blacklist(char *s)
{
  for(size_t i = 0; i<blacklist_len; i++)
  {
    if(strcmp(blacklist[i], s) == 0)
    {
      return 1;
    }
  }
  return 0;
}

static void sort_langs(analyse_result_t *result)
{
  if(result->langs->length < 2)
    return;
  unsigned int actions = 1;

  node_t *last_node = result->langs->head;
  struct lang_info *last = (struct lang_info *) last_node->value;
  struct lang_info *curr;
  node_t *n;
  while(actions > 0)
  {
    actions = 0;
    last_node = list_start_iterating(result->langs);
    last = (struct lang_info *) last_node->value;

    while((n = list_iterate(result->langs)) != NULL)
    {
      curr = (struct lang_info *) n->value;
      if(curr->lines > last->lines)
      {
        list_swap_node(n, last_node);
        actions++;
      }
      last_node = n;
      last = curr;
    }
  }
}

static void process_results(analyse_result_t *result, struct options *opt, char *langs, size_t langs_len)
{
  langs[0] = '\0';

  sort_langs(result);

  foreach(result->langs, n)
  {
    struct lang_info *li = (struct lang_info *) n->value;

    /* calculate percentage */
    li->percent = ((float) li->lines) / ((float) result->total_lines) * 100.0;
    foreach(li->files, nf)
    {
      struct file_info *fi = (struct file_info *) nf->value;
      fi->percent = ((float) fi->lines) / ((float) li->lines) * 100.0;
    }

    /* nice string with every languages */
    if((strlen(langs) + 64) < langs_len)
    {
      char buf[64];
      snprintf(buf, 64, "%s (%.2f%%), ", common_langs[li->lang].name, li->percent);
      strcat(langs, buf);
    }
  }
  langs[strlen(langs) - 2] = '\0';
}

void dump_result(analyse_result_t *result, struct options *opt)
{
  printf("\nResults for \"%s\":\n\n", result->project_folder);

  char langs[512];
  process_results(result, opt, langs, 512);

  /* Global statistics */
  printf("Global statistics :\n");
  printf("  Total lines : %lu\n", result->total_lines);
  printf("  Number of files : %lu", result->total_files);
  printf(" (Excluding binary files : %lu file%s)\n", result->binary_files, (result->binary_files == 1) ? "" : "s");
  printf("  Languages : %s\n", langs);

  /* Per language statistics */
  if(opt->details != NULL)
  {
    unsigned int i;
    for(i = 0; i<common_langs_len; i++)
    {
      struct lang_data *ld = &(common_langs[i]);
      if(is_in_details(opt->details, ld))
      {
        dump_detail(ld, i, result);
      }
    }
  }
}
