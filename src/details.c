#include <analyse.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* strcasestr function is not standard (which is why we need to define _GNU_SOURCE) */

int is_in_alias(char *s, char *alias)
{
  return strcasestr(alias, s) != NULL;
}

static char details_tmp[256];
int is_in_details(char *details, struct lang_data *lang)
{
  strncpy(details_tmp, details, 255);

  char *saveptr;
  char *token = strtok_r(details_tmp, ",", &saveptr);

  do {
    if(strcasecmp(token, lang->name) == 0 || strcasestr(lang->alias, token) != NULL)
    {
      return 1;
    }
    token = strtok_r(NULL, ",", &saveptr);
  } while(token != NULL);

  return 0;
}
