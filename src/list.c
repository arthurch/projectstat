#include <list.h>

#include <malloc.h>

#include <stdio.h>

#define OWNER(n) ((list_t *) n->owner)

list_t *list_create()
{
  list_t *l = (list_t *) malloc(sizeof(list_t));

  l->length = 0;
  l->head = NULL;
  l->tail = NULL;
  l->curr = NULL;
  l->reiterate = 1;
  return l;
}

node_t *list_create_node(void *value)
{
  node_t *node = (node_t *) malloc(sizeof(node_t));
  node->bwd = NULL;
  node->fwd = NULL;
  node->owner = NULL;
  node->value = value;
  return node;
}

node_t *list_append_node(list_t *l, node_t *n)
{
  if(__builtin_expect(l == NULL, 0) || __builtin_expect(n == NULL, 0))
  {
    return NULL;
  }

  if(l->tail != NULL)
  {
    node_t *last = l->tail;
    last->fwd = n;
    l->tail = n;
    n->bwd = last;
  } else
  {
    l->tail = n;
    l->head = n;
  }
  n->owner = l;
  l->length++;

  return n;
}

void list_reset_iterating(list_t *l)
{
  l->curr = NULL;
  l->reiterate = 1;
}

node_t *list_start_iterating(list_t *l)
{
  l->curr = l->head;
  l->reiterate = 1;
  return l->curr;
}

node_t *list_iterate(list_t *l)
{
  if(l->length == 0)
  {
    return NULL;
  }
  if(l->curr == NULL)
  {
    l->curr = l->head;
    return l->curr;
  }
  if(!l->reiterate)
  {
    l->reiterate = 1;
    return l->curr;
  }

  node_t *next = l->curr->fwd;
  l->curr = next;
  return next;
}

node_t *list_append(list_t *l, void *value)
{
  return list_append_node(l, list_create_node(value));
}

void list_swap_node(node_t *n1, node_t *n2)
{
  if(n1->owner != n2->owner)
    return;
  void *tmp = n1->value;
  n1->value = n2->value;
  n2->value = tmp;
}

void list_remove(list_t *l, void *value)
{
  foreach(l, n)
  {
    if(n->value == value)
    {
      list_remove_node(n);
      return;
    }
  }
}

void list_remove_node(node_t *n)
{
  if(__builtin_expect(n == NULL, 0))
  {
    return;
  }

  if(n->owner == NULL)
    return;
  if(n->bwd != NULL)
  {
    n->bwd->fwd = n->fwd;
  } else
  {
    /* first element of the list */
    OWNER(n)->head = n->fwd;
  }
  if(n->fwd != NULL)
  {
    n->fwd->bwd = n->bwd;
  } else
  {
    /* last element of the list */
    OWNER(n)->tail = n->bwd;
  }

  OWNER(n)->length--;

  if(n->owner->curr == n)
  {
    n->owner->curr = n->fwd;
    n->owner->reiterate = 0;
  }

  free(n);
}

node_t *list_find(list_t *l, void *value)
{
  foreach(l, node)
  {
    if(node->value == value)
    {
      return node;
    }
  }
  return NULL;
}

node_t *list_find_custom(list_t *l, void *compare_to, list_compare_t compare)
{
  foreach(l, node)
  {
    if((*compare)(node->value, compare_to) == 1)
    {
      return node;
    }
  }
  return NULL;
}

void list_free_node(node_t *n)
{
  free(n->value);
  list_remove_node(n);
}

void list_empty(list_t *l)
{
  node_t *next = l->head;
  node_t *n;
  while(next != NULL)
  {
    n = next;
    next = n->fwd;
    free(n);
  }
  l->head = NULL;
  l->tail = NULL;
  l->length = 0;
}

void list_free(list_t *l)
{
  node_t *next = l->head;
  node_t *n;
  while(next != NULL)
  {
    n = next;
    next = n->fwd;
    free(n);
  }
  free(l);
}

void list_destroy(list_t *l)
{
  foreach(l, node)
  {
    free(node->value);
  }
  list_free(l);
}

void list_dump(list_t *l)
{
  printf("(%p) ", l);
  foreach(l, n)
  {
    printf("%p:%p,", n, n->value);
  }
  printf("\n");
}
