#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>

#include <stdio.h>
#include <stdlib.h>

#include <conf.h>

#define debug_printf(...) \
  if(DEBUG_ALL_LOGS) { printf(__VA_ARGS__); }

void error(char *reason);

int strendswith(const char *src, const char *end);

int is_binary(const void *data, size_t len);

size_t filelines(FILE *f);
int fileisbin(FILE *f);
size_t filesize(FILE *f);

#endif
