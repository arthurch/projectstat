#ifndef LIST_H
#define LIST_H

#include <stdint.h>
#include <stddef.h>

#define foreach(list, n) for(node_t *n = (list)->head; n != NULL; n = n->fwd)
#define foreachr(list, n) for(node_t *n = (list)->tail; n != NULL; n = n->bwd)

typedef uint8_t (*list_compare_t)(void *val, void *compare_to);

struct node {
  struct node *bwd;
  struct node *fwd;
  void *value;

  struct list *owner;
};
typedef struct node node_t;

struct list {
  size_t length;

  node_t *curr;
  uint8_t reiterate; /* tell list_iterate not to iterate next time */

  node_t *head;
  node_t *tail;
};
typedef struct list list_t;

list_t *list_create();
node_t *list_create_node(void *value);

node_t *list_append_node(list_t *l, node_t *n);
node_t *list_append(list_t *l, void *value);

void list_swap_node(node_t *n1, node_t *n2);

node_t *list_iterate(list_t *l);
node_t *list_start_iterating(list_t *l);
void list_reset_iterating(list_t *l);

node_t *list_find(list_t *l, void *value);
node_t *list_find_custom(list_t *l, void *compare_to, list_compare_t compare);

void list_remove_node(node_t *n);
void list_remove(list_t *l, void *value);
void list_free_node(node_t *n);

void list_empty(list_t *l);
/* free list and nodes */
void list_free(list_t *l);
/* free list and nodes AND value pointers */
void list_destroy(list_t *l);

void list_dump(list_t *l);

#endif
