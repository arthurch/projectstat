#ifndef ANALYSE_H
#define ANALYSE_H

#include <stdint.h>
#include <stddef.h>

#include <list.h>

struct options {
  uint8_t quiet;
  char *details;
};

struct file_info {
  char path[256];
  size_t type;
  size_t lines;

  float percent;
};

struct lang_info {
  size_t lang;
  size_t lines;
  size_t files_nb;

  float percent;

  list_t *files;
};

struct analyse_result {
  char *project_folder;

  list_t *langs;
  size_t total_langs;

  size_t total_lines;
  size_t total_files;

  size_t unknown_lines;
  size_t unknown_files;

  size_t binary_files;
};
typedef struct analyse_result analyse_result_t;

struct lang_data {
  const char *name;
  char *alias;
};

struct file_data {
  const char *ext;
  int is_binary;
  int lang;
};

extern struct file_data common_files[];
extern size_t common_files_len;
extern struct lang_data common_langs[];
extern size_t common_langs_len;
extern const char *blacklist[];
extern size_t blacklist_len;

void analyse(char *path, analyse_result_t *result, struct options *opt);
void free_result(analyse_result_t *result);
void dump_result(analyse_result_t *result, struct options *opt);

void dump_detail(struct lang_data *lang, size_t langid, analyse_result_t *result);

int is_in_alias(char *s, char *alias);
int is_in_details(char *details, struct lang_data *lang);

int is_in_blacklist(char *s);

#endif
