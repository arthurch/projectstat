#ifndef COMMON_H
#define COMMON_H

#define BINARY              0
#define LANGUAGE_C          1
#define LANGUAGE_ASM        2
#define LANGUAGE_JAVA       3
#define LANGUAGE_SHELL      4
#define LANGUAGE_MAKEFILE   5
#define LANGUAGE_CPP        6
#define LANGUAGE_CSHARP     7
#define LANGUAGE_JAVASCRIPT 8
#define LANGUAGE_JSON       9
#define LANGUAGE_PYTHON     10
#define LANGUAGE_RUST       11
#define LANGUAGE_PERL       12
#define LANGUAGE_PHP        13
#define LANGUAGE_HTML       14
#define LANGUAGE_CSS        15
#define LANGUAGE_RUBY       16
#define LANGUAGE_LUA        17
#define LANGUAGE_MARKDOWN   18
#define LANGUAGE_LINK       19
#define LANGUAGE_VBNET      20
#define LANGUAGE_OBJC       21
#define LANGUAGE_SQL        22

#endif
