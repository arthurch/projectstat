# Configuration
OUTPUT:=projectstat
INSTALL_TO:=/usr/bin
CC:=gcc
ALWAYS_CLEAN:=true

# Build options
CFLAGS:=-std=gnu11 -g -O2 -Wall -Iinclude/ -D_GNU_SOURCE
LFLAGS:=

# Files to be built
CFILES:=$(shell find src -name *.c)
OBJS:=$(CFILES:.c=.o)

.PHONY: all install clean cleanall

all: $(OUTPUT)
ifeq (true, $(ALWAYS_CLEAN))
	make clean
endif

install: $(OUTPUT)
	@echo "Installing software to $(INSTALL_TO) (may need super user level)"
	@cp $(OUTPUT) $(INSTALL_TO)/$(OUTPUT)
	@echo "Done."

# Final output
$(OUTPUT): $(OBJS)
	$(CC) -o $(OUTPUT) $(LFLAGS) $^

# Common rules
%.o: %.c Makefile
	$(CC) -c -o $@ $(CFLAGS) $<

clean:
	@rm -f src/*.o

cleanall: clean
	@rm -f $(OUTPUT)
